from django.views.generic import TemplateView 

from cao import models as cao_models

import json
class HomeTemplateView(TemplateView):
	template_name = "home.html"
	def post(self, request, *args, **kwargs):
		context = self.get_context_data()
		return super(TemplateView, self).render_to_response(context)

	def get_context_data(self, **kwargs):
		context = super(HomeTemplateView, self).get_context_data(**kwargs)
		context["type"] = self.request.POST.get("type", "relatorio")
		search = self.request.POST.getlist("to", [])

		#Filtro los Permisos por in_ativo, tipo_usuaruo, co_sistema y obtengo los co_usuario a los que pertencen
		pS = cao_models.PermissaoSistema.objects.filter(in_ativo= 'S',co_sistema = 1,co_tipo_usuario__in=[0,1,2])
		pS_co_usuario = [x.co_usuario for x in pS]

		#Obtengo lista de Usuarios de la dB y parseo datos iniciales
		context["users"] = {}
		users = cao_models.CaoUsuario.objects.filter(co_usuario__in=pS_co_usuario)
		name_dicts = {}
		for user in users:
			if user.no_usuario != "":
				context["users"][user.co_usuario] =  {'user': user}
				context["users"][user.co_usuario]["brut_salario"] = 0
				context["users"][user.co_usuario]["name"] = user.no_usuario
				context["users"][user.co_usuario]["co_usuario"] = user.co_usuario
				name_dicts[user.co_usuario] = context["users"][user.co_usuario]["name"]
							

		#obtengo el brut_salario de cada usuario y se lo asigno
		brut_salarios = {}
		caoSalarios = cao_models.CaoSalario.objects.filter(co_usuario__in=pS_co_usuario)
		for cS in caoSalarios:
			if user.no_usuario != "":
				context["users"][cS.co_usuario]["brut_salario"] = cS.brut_salario 

		#Obtener solos facturas de los que seleccione.
		context["to"] = []
		users_dict_search = {}
		for user in search:
			users_dict_search[user] =  {'user': context["users"][user]["user"]}
			context["to"].append(context["users"][user])


		#Consulto todos los CaoOs de Todo los Usuarios
		co_usuario_list = users_dict_search.keys()
		cao_os = cao_models.CaoOs.objects.filter(co_usuario__in=co_usuario_list) #NOTA: (He usado __in para acelerar las respuesta ya que si uso get 1 a 1 todo es muy lento.)
		co_os_dict = {}
		for x in cao_os:
			co_os_dict[x.co_os] = x.co_usuario

		
		#Consulto Todas las Facturas de Todos los CaOs
		user_invoices = {}
		date_range = self.request.POST.getlist("date", [])
		if date_range:
			date_range_start = date_range[0].split(" - ")[0]
			date_range_end = date_range[0].split(" - ")[1]
			date_range_start = '-'.join(reversed(date_range_start.split("-")))
			date_range_end = '-'.join(reversed(date_range_end.split("-")))
			date_range = [date_range_start, date_range_end]
		else:
			date_range = []

		#Consultos todas las facuras de todos los co_os segun elr ango de fecha determinada.
		invoices_objects = cao_models.CaoFatura.objects.filter(co_os__in=co_os_dict.keys()).filter(data_emissao__range=date_range).order_by("data_emissao") #NOTA: (He usado __in para acelerar las respuesta ya que si uso get 1 a 1 todo es muy lento.)
		for y in invoices_objects:

			#Obtengo el Usuario al que pertenece dicha factura, sin consultar en DB
			co_usuario = co_os_dict[y.co_os]

			if co_usuario not in user_invoices:
				user_invoices[co_usuario] = {"receita_liquida_total": 0,"brut_salario_total": 0,"comissao_cn_total": 0, "lucro_total": 0, "invoices": {}}
			#Realizo el calculo de un nuevo valor con (valor y total_imp_inc)
			receita_liquida_fatura = (y.valor - (y.valor * y.total_imp_inc)/100)
			y_dict = y.__dict__
			y_dict["receita_liquida_fatura"] = receita_liquida_fatura

			y_dict["comissao_cn_fatura"] = receita_liquida_fatura * y.comissao_cn/100

			#Obtengo un identificador para las facturas de cierta fecha.
			date_identifier = "%s-%s" % (y.data_emissao.month, y.data_emissao.year)
			if date_identifier in user_invoices[co_usuario]["invoices"]:
				user_invoices[co_usuario]["invoices"][date_identifier]["receita_liquida"] = user_invoices[co_usuario]["invoices"][date_identifier]["receita_liquida"] + y_dict["receita_liquida_fatura"]
				user_invoices[co_usuario]["invoices"][date_identifier]["comissao_cn"] = user_invoices[co_usuario]["invoices"][date_identifier]["comissao_cn"] + y_dict["comissao_cn_fatura"]
				user_invoices[co_usuario]["invoices"][date_identifier]["CaoFatura"].append(y.__dict__)

			else:
				user_invoices[co_usuario]["invoices"][date_identifier] = {}
				user_invoices[co_usuario]["invoices"][date_identifier]["receita_liquida"] = y_dict["receita_liquida_fatura"]
				user_invoices[co_usuario]["invoices"][date_identifier]["comissao_cn"] = y_dict["comissao_cn_fatura"]
				user_invoices[co_usuario]["invoices"][date_identifier]["CaoFatura"] = [y.__dict__]
				user_invoices[co_usuario]["invoices"][date_identifier]["co_usuario"] = co_usuario
				user_invoices[co_usuario]["invoices"][date_identifier]["brut_salario"] = context["users"][co_usuario]["brut_salario"]

		for co_usuario, invoices in user_invoices.items():
			for invoice in invoices["invoices"].values():
				invoice["lucro"] = invoice["receita_liquida"] - (context["users"][co_usuario]["brut_salario"] + invoice["comissao_cn"])
				invoices["receita_liquida_total"] += invoice["receita_liquida"]
				invoices["brut_salario_total"] += context["users"][co_usuario]["brut_salario"]
				invoices["comissao_cn_total"] += invoice["comissao_cn"]
				invoices["lucro_total"] += invoice["lucro"]

		import operator
		data = {"count": len(user_invoices), "data":{}}


		#Inserto a cada usuario las facturas que le pertenecen y genero dara para crear informacion de graficos.
		corechart2 = []
		for co_usuario,invoices in user_invoices.items():
			context["users"][co_usuario]["invoices"] =  invoices
			corechart2.append([name_dicts[co_usuario],invoices["receita_liquida_total"]])
			for key, value in invoices["invoices"].items():
				if key not in data["data"]:
					data["data"][key] = [value]
				else:
					data["data"][key].append(value)

		# genro toda la informacion de los graficos
		corechart = [["mes"]]	
		for date, values in data["data"].items():
			for x in values:
				if name_dicts[x["co_usuario"]] not in corechart[0]:
					corechart[0].append(name_dicts[x["co_usuario"]])

		for date, values in data["data"].items():
			fecha = date.replace("-","/")
			lista = [fecha]
			for x in range(data["count"]):
				lista.append(0)				
			for x in values:
				index = corechart[0].index(name_dicts[x["co_usuario"]])
				lista[index] = x["receita_liquida"]
			brut_salario = [x["brut_salario"]/len(values) for x in values][-1]
			lista.append(brut_salario)
			corechart.append(lista)

		corechart[0].append("Custo Fixo Medio")
		context["users"]["corechart_count"] = data["count"]
		context["users"]["corechart"] = json.dumps(corechart)
		context["users"]["corechart2"] = json.dumps(corechart2)
		return context